package com.campo.jose.cuentaganadoapp.bean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Elemento implements Serializable {
    protected int id;
    protected String nombre;
    protected Date fecha;
    protected List<String> fotos;
    protected long estId;
    protected String  descripcion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getEstId() {
        return estId;
    }

    public void setEstId(long estId) {
        this.estId = estId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        DateFormat df = new SimpleDateFormat();
        return df.format( fecha).toString();
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List getFotos() {
        return fotos;
    }
    public void setFotos(List<String> fotos) {
        this.fotos=new ArrayList<>(fotos);
    }

    public void addFoto(String foto) {
        this.fotos.add(foto);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String toString(){
        return this.nombre;
    }
}
