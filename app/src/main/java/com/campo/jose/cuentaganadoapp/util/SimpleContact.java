package com.campo.jose.cuentaganadoapp.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

public class SimpleContact {

    private String contactId;
    private String displayName;
    private String photo;
    private String lookupKey;
    private String phoneNumber;

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public SimpleContact(String contactId){
        this.contactId=contactId;

    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public static SimpleContact getLocalProfile(Context context) {

        Uri PROFILE_URI = ContactsContract.Profile.CONTENT_URI;
        String[] PROJECTION = new String[]{
                ContactsContract.Profile._ID,
                ContactsContract.Profile.DISPLAY_NAME,
                ContactsContract.Profile.PHOTO_URI,
                ContactsContract.Profile.HAS_PHONE_NUMBER,
                ContactsContract.Profile.LOOKUP_KEY
        };

        Cursor c = context.getContentResolver().query(PROFILE_URI, PROJECTION, null, null, null);

        SimpleContact item = null;

        if (c != null && c.moveToFirst()) {
            String contactId = c.getString(c.getColumnIndex(ContactsContract.Profile._ID));
            item = new SimpleContact(contactId);
            item.setDisplayName(c.getString(c.getColumnIndex(ContactsContract.Profile.DISPLAY_NAME)));
            item.setPhoto(c.getString(c.getColumnIndex(ContactsContract.Profile.PHOTO_URI)));
            item.setLookupKey(c.getString(c.getColumnIndex(ContactsContract.Profile.LOOKUP_KEY)));

            if (c.getInt(c.getColumnIndex(ContactsContract.Profile.HAS_PHONE_NUMBER)) > 0) {

                Cursor pCur = context.getContentResolver().query(
                        // Retrieves data rows for the device user's 'profile' contact
                        Uri.withAppendedPath(
                                ContactsContract.Profile.CONTENT_URI,
                                ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
                        new String[]{
                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                ContactsContract.CommonDataKinds.Phone.IS_PRIMARY,
                                ContactsContract.Contacts.Data.MIMETYPE
                        },

                        // Selects only email addresses or names
                        ContactsContract.Contacts.Data.MIMETYPE + "=? ",
                        new String[]{
                                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
                        },

                        // Show primary rows first. Note that there won't be a primary email address if the
                        // user hasn't specified one.
                        ContactsContract.Contacts.Data.IS_PRIMARY + " DESC"
                );

                if (pCur != null && pCur.moveToFirst()) {
                    item.setPhoneNumber(pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                    pCur.close();
                }

            }
            c.close();
        }

        return item;

    }
}
