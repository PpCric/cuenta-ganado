package com.campo.jose.cuentaganadoapp.bean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jose on 24/08/17.
 */

public class Cuenta implements Serializable{

    private int idCuenta;
    private int cantidad;
    private String nombre;
    private Date fecha;
    private String descripcion;
    private long estId;

    public Cuenta() {
        this.cantidad = 0;
        this.fecha = new Date();
    }
    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public long getEstId() {
        return estId;
    }

    public void setEstId(long estId) {
        this.estId = estId;
    }

    public void contar(){
        cantidad++;
    }
    public String getCantidad(){
        return Integer.toString(cantidad);
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        DateFormat df = new SimpleDateFormat();
        return df.format( fecha).toString();
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String toString(){
        if (this.nombre!=null)
        return this.nombre;
        else return "Sin Nombre";
    }
}
