package com.campo.jose.cuentaganadoapp.activity;

import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.campo.jose.cuentaganadoapp.AppConstant;
import com.campo.jose.cuentaganadoapp.R;
import com.campo.jose.cuentaganadoapp.adapter.FotoAdapter;
import com.campo.jose.cuentaganadoapp.bean.Elemento;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;

import java.util.ArrayList;

public class FotosListActivity extends AppCompatActivity {

    private GridView gridView = null;
    private Elemento elemento = null;
    private Establecimiento establecimiento = null;
    private String action = null;
    private String choice = null;
    private ArrayList<String> fotosList = null;
    private FotoAdapter fAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fotos_lista);

        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        Bundle datos = getIntent().getExtras();
        this.elemento = (Elemento) datos.getSerializable(AppConstant.ELEMENTO);
        this.establecimiento = (Establecimiento) datos.getSerializable(AppConstant.ESTABLECIMIENTO);
        this.action = datos.getString(AppConstant.ACTION);
        this.choice = datos.getString(AppConstant.CHOICE);

        toolbar.setSubtitle(this.elemento.getNombre());
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        fotosList =  (ArrayList<String>) this.elemento.getFotos();

        gridView = (GridView) findViewById(R.id.gridView);

        fAdapter = new FotoAdapter(this,R.layout.fotos_items,fotosList, getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath());

        gridView.setAdapter(fAdapter);

        fAdapter.notifyDataSetChanged();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent intent = new Intent(getApplicationContext(),FotoViewerActivity.class);
                intent.putExtra("foto",fotosList.get(position));
                intent.putExtra(AppConstant.ELEMENTO, elemento);
                intent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                intent.putExtra(AppConstant.ACTION,action);
                intent.putExtra(AppConstant.CHOICE,choice);
                startActivityForResult(intent, 0);

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.putExtra(AppConstant.ELEMENTO, elemento);
                upIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                upIntent.putExtra(AppConstant.ACTION,action);
                upIntent.putExtra(AppConstant.CHOICE,choice);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
