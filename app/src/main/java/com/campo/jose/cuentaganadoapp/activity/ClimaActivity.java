package com.campo.jose.cuentaganadoapp.activity;


import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.campo.jose.cuentaganadoapp.AppConstant;
import com.campo.jose.cuentaganadoapp.R;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;

import org.json.JSONException;
import org.json.JSONObject;

public class ClimaActivity extends AppCompatActivity {

    private Establecimiento establecimiento= null;
    private TextView condicion;
    private TextView temperatura;
    private TextView viento;
    private TextView gradoHectopascales;
    private TextView vientoDireccion;
    private TextView presionAtmosferica;
    private TextView precipitaciones;
    private TextView humedad;
    private TextView nubosidad;
    private TextView sensacionTermica;
    private TextView visibilidad;

    private ImageView imageView;
    private ProgressBar progressBar;
    private StringRequest stringRequest;
    private RequestQueue queue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clima);

        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null

        Bundle datos = getIntent().getExtras();
        this.establecimiento = (Establecimiento) datos.getSerializable(AppConstant.ESTABLECIMIENTO);

        toolbar.setSubtitle(this.establecimiento.getNombre());

        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        double latitud = this.establecimiento.getCoordenadas().latitude;
        double longitud = this.establecimiento.getCoordenadas().longitude;
        condicion = (TextView) findViewById(R.id.condicion);
        imageView = (ImageView) findViewById(R.id.imageViewClima);

        temperatura = (TextView) findViewById(R.id.temperatura);
        viento = (TextView) findViewById(R.id.viento);
        gradoHectopascales = (TextView) findViewById(R.id.viento_grados);
        vientoDireccion = (TextView) findViewById(R.id.viento_direccion);
        presionAtmosferica = (TextView) findViewById(R.id.presion);
        precipitaciones = (TextView) findViewById(R.id.precipitaciones);
        humedad = (TextView) findViewById(R.id.humedad);
        nubosidad = (TextView) findViewById(R.id.nubosidad);
        sensacionTermica = (TextView) findViewById(R.id.sensaciontermica);
        visibilidad = (TextView) findViewById(R.id.visibilidad);
        // Instantiate the RequestQueue.

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        queue = Volley.newRequestQueue(this);

        String urlLocal = "http://api.apixu.com/v1/current.json?key="+AppConstant.APIXUKEY+"&q="+latitud+","+longitud+"&lang=es";
        // Request a string response from the provided URL.
        stringRequest = new StringRequest(Request.Method.GET, urlLocal, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonObj=null;
                        String imageURL = null;
                        String condicion = null;
                        String temperatura = null;
                        String viento = null;
                        String gradoHectopascales = null;
                        String vientoDireccion = null;
                        String presionAtmosferica = null;
                        String precipitaciones = null;
                        String humedad = null;
                        String nubosidad = null;
                        String sensacionTermica = null;
                        String visibilidad = null;
                        try {
                            jsonObj = new JSONObject(response);
                            imageURL = ((JSONObject)((JSONObject)jsonObj.get("current")).get("condition")).getString("icon");
                            condicion = ((JSONObject)((JSONObject)jsonObj.get("current")).get("condition")).getString("text");
                            temperatura = ((JSONObject)(jsonObj.get("current"))).getString("temp_c");
                            viento = ((JSONObject)(jsonObj.get("current"))).getString("wind_kph");
                            gradoHectopascales = ((JSONObject)(jsonObj.get("current"))).getString("wind_degree");
                            vientoDireccion = ((JSONObject)(jsonObj.get("current"))).getString("wind_dir");
                            presionAtmosferica = ((JSONObject)(jsonObj.get("current"))).getString("pressure_in");
                            precipitaciones = ((JSONObject)(jsonObj.get("current"))).getString("precip_mm");
                            humedad = ((JSONObject)(jsonObj.get("current"))).getString("humidity");
                            nubosidad = ((JSONObject)(jsonObj.get("current"))).getString("cloud");
                            sensacionTermica = ((JSONObject)(jsonObj.get("current"))).getString("feelslike_c");
                            visibilidad = ((JSONObject)(jsonObj.get("current"))).getString("vis_km");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        cargarInfo(imageURL ,condicion , temperatura ,viento ,gradoHectopascales ,vientoDireccion ,presionAtmosferica ,precipitaciones , humedad , nubosidad ,sensacionTermica , visibilidad );

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                condicion.setText("Hay problemas en la conexion");
            }
        });
        queue.add(stringRequest);
    }
    private void cargarInfo(String imageURL, String condicion ,String temperatura ,String viento ,String gradoHectopascales ,String vientoDireccion ,String presionAtmosferica ,String precipitaciones ,String humedad ,String nubosidad ,String sensacionTermica ,String visibilidad ){
        Glide.with(getApplicationContext()).load("http:"+imageURL)
                .centerCrop()
                .fitCenter()
                .into(imageView);
        this.progressBar.setVisibility(View.INVISIBLE);
        this.condicion.setText(this.condicion.getText()+": "+condicion);
        this.temperatura.setText(this.temperatura.getText()+": "+temperatura);
        this.viento.setText(this.viento.getText()+": "+viento);
        this.gradoHectopascales.setText(this.gradoHectopascales.getText()+": "+gradoHectopascales);
        this.vientoDireccion.setText(this.vientoDireccion.getText()+": "+vientoDireccion);
        this.presionAtmosferica.setText(this.presionAtmosferica.getText()+": "+presionAtmosferica);
        this.precipitaciones.setText(this.precipitaciones.getText()+": "+precipitaciones);
        this.humedad.setText(this.humedad.getText()+": "+humedad);
        this.nubosidad.setText(this.nubosidad.getText()+": "+nubosidad);
        this.sensacionTermica.setText(this.sensacionTermica.getText()+": "+sensacionTermica);
        this.visibilidad.setText(this.visibilidad.getText()+": "+visibilidad);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.putExtra(AppConstant.ESTABLECIMIENTO,this.establecimiento);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
