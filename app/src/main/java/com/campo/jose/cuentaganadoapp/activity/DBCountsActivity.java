package com.campo.jose.cuentaganadoapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.campo.jose.cuentaganadoapp.AppConstant;
import com.campo.jose.cuentaganadoapp.R;
import com.campo.jose.cuentaganadoapp.bd.CuentaGanadoDbHelper;
import com.campo.jose.cuentaganadoapp.bean.Cuenta;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;

import java.util.ArrayList;

public class DBCountsActivity extends AppCompatActivity {

    private ListView li;
    private ArrayList<Cuenta> lista;
    private Establecimiento establecimiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dblist);

        Bundle datos = getIntent().getExtras();
        this.establecimiento = (Establecimiento) datos.getSerializable(AppConstant.ESTABLECIMIENTO);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setSubtitle(establecimiento.getNombre());
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        li = (ListView) findViewById(R.id.listCount);
        CuentaGanadoDbHelper db = new CuentaGanadoDbHelper(this);

        lista = db.getCuentas( establecimiento.getId());
        ArrayAdapter<Cuenta> adapter = new ArrayAdapter<Cuenta>(this,android.R.layout.simple_list_item_1,lista);
        li.setAdapter(adapter);

        li.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                Cuenta c = lista.get(position);
                Intent myIntent = new Intent(view.getContext(), CountInfoActivity.class);
                myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                myIntent.putExtra(AppConstant.CUENTA,c);
                myIntent.putExtra(AppConstant.ACTION,AppConstant.EDIT);
                startActivityForResult(myIntent, 0);
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent myIntent = new Intent(view.getContext(), CountActivity.class);
                myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                Cuenta c = new Cuenta();
                c.setEstId(establecimiento.getId());
                myIntent.putExtra(AppConstant.CUENTA,c);
                myIntent.putExtra(AppConstant.ACTION,AppConstant.CREATE);
                startActivityForResult(myIntent, 0);
                /*
                Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                */
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.putExtra(AppConstant.ESTABLECIMIENTO,this.establecimiento);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
