package com.campo.jose.cuentaganadoapp;

/**
 * Created by jbordagaray on 17/11/17.
 */

public class AppConstant {
    public static String ELEMENTO = "elemento";
    public static String CUENTA = "cuenta";
    public static String ANIMAL = "animal";
    public static String DANIO = "danio";
    public static String ESTABLECIMIENTO = "establecimiento";

    public static String CHOICE = "choice";
    public static String ACTION = "action";
    public static String EDIT = "editar";
    public static String CREATE = "crear";

    public static String APIWEATHERKEY = "aff145fadea382e7"; //www.wunderground.com
    public static String APIXUKEY = "f3706ae985d04fd09fc163024180503";//http://api.apixu.com
}
