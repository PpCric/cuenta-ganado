package com.campo.jose.cuentaganadoapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


import com.campo.jose.cuentaganadoapp.AppConstant;
import com.campo.jose.cuentaganadoapp.R;
import com.campo.jose.cuentaganadoapp.bean.Cuenta;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;

public class CountActivity extends AppCompatActivity {

    private Button contar = null;
    private Button fin = null;
    private Cuenta cuenta = null;
    private Establecimiento establecimiento = null;
    private String action = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count);
        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null

        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Bundle datos = getIntent().getExtras();
        this.establecimiento = (Establecimiento) datos.getSerializable(AppConstant.ESTABLECIMIENTO);
        this.cuenta = (Cuenta)datos.getSerializable("cuenta");
        this.action = datos.getString("action");


        contar = (Button)findViewById(R.id.countButton);
        contar.setText(cuenta.getCantidad());
        contar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               cuenta.contar();
                contar.setText(cuenta.getCantidad());
            }
        });

        fin = (Button) findViewById(R.id.finButton);
        fin.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent myIntent = new Intent(v.getContext(), CountInfoActivity.class);
                myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                myIntent.putExtra(AppConstant.CUENTA,cuenta);
                myIntent.putExtra(AppConstant.ACTION,action);
                startActivityForResult(myIntent, 0);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.putExtra(AppConstant.ESTABLECIMIENTO,this.establecimiento);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
