package com.campo.jose.cuentaganadoapp.bean;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Created by jose on 28/08/17.
 */

public class Establecimiento implements Serializable {
    private int id;
    private String nombre;
    private double latitud;
    private double longitud;
    private String descripcion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LatLng getCoordenadas() {
        return new LatLng(latitud,longitud);
    }

    public void setCoordenadas(LatLng coordenadas) {

        this.longitud = coordenadas.longitude;
        this.latitud = coordenadas.latitude;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String toString(){
        return id+"- " +nombre;
    };
}
