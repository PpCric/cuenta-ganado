package com.campo.jose.cuentaganadoapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.campo.jose.cuentaganadoapp.R;

import java.util.ArrayList;

/**
 * Created by jbordagaray on 11/01/18.
 */

public class FotoAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList fotosList;
    private String mCurrentPhotoPath;

    public FotoAdapter(Context context, int layout, ArrayList<String> fotosList, String path) {
        this.context = context;
        this.layout = layout;
        this.fotosList = fotosList;
        this.mCurrentPhotoPath = path;
    }

    @Override
    public int getCount() {
        return fotosList.size();
    }

    @Override
    public Object getItem(int position) {
        //Ojo aca
        return fotosList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        ImageView imageView;
    }
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View row = view;
        ViewHolder holder = new ViewHolder();
        if (row == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout,null);
            holder.imageView = (ImageView) row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        String foto = (String)fotosList.get(position);

        Glide.with(context).load(mCurrentPhotoPath+"/" + foto)
                .into(holder.imageView);


        return row;
    }


}
