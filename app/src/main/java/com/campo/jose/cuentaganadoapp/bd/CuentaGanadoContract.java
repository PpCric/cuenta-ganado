package com.campo.jose.cuentaganadoapp.bd;

import android.provider.BaseColumns;

/**
 * Created by jose on 29/08/17.
 */

public final class CuentaGanadoContract {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    public static final String SQL_CREATE_ESTABLECIMIENTO =
            "CREATE TABLE " + Establecimiento.TABLE_NAME + " (" +
                    Establecimiento._ID + " INTEGER PRIMARY KEY," +
                    Establecimiento.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP +
                    Establecimiento.COLUMN_NAME_LATITUD+ TEXT_TYPE + COMMA_SEP +
                    Establecimiento.COLUMN_NAME_LONGITUD+ TEXT_TYPE + COMMA_SEP +
                    Establecimiento.COLUMN_NAME_DESCRIPCION+ TEXT_TYPE + " )";

    public static final String SQL_CREATE_ANIMALENFERMO =
            "CREATE TABLE " + AnimalEnfermo.TABLE_NAME + " (" +
                    AnimalEnfermo._ID + " INTEGER PRIMARY KEY," +
                    AnimalEnfermo.EST_ID + TEXT_TYPE + COMMA_SEP +
                    AnimalEnfermo.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP +
                    AnimalEnfermo.COLUMN_NAME_FECHA + TEXT_TYPE + COMMA_SEP +
                    AnimalEnfermo.COLUMN_NAME_FOTO+ TEXT_TYPE + COMMA_SEP +
                    AnimalEnfermo.COLUMN_NAME_DESCRIPCION+ TEXT_TYPE + " )";

    public static final String SQL_CREATE_CUENTA =
            "CREATE TABLE " + Cuenta.TABLE_NAME + " (" +
                    Cuenta._ID + " INTEGER PRIMARY KEY," +
                    Cuenta.EST_ID + TEXT_TYPE + COMMA_SEP +
                    Cuenta.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP +
                    Cuenta.COLUMN_NAME_FECHA + TEXT_TYPE + COMMA_SEP +
                    Cuenta.COLUMN_NAME_CUENTA+ TEXT_TYPE + COMMA_SEP +
                    Cuenta.COLUMN_NAME_DESCRIPCION+ TEXT_TYPE + " )";

    public static final String SQL_CREATE_DANIO =
            "CREATE TABLE " + Danio.TABLE_NAME + " (" +
                    Danio._ID + " INTEGER PRIMARY KEY," +
                    Danio.EST_ID + TEXT_TYPE + COMMA_SEP +
                    Danio.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP +
                    Danio.COLUMN_NAME_FECHA + TEXT_TYPE + COMMA_SEP +
                    Danio.COLUMN_NAME_FOTO + TEXT_TYPE + COMMA_SEP +
                    Danio.COLUMN_NAME_DESCRIPCION+ TEXT_TYPE + " )";

    public static final String SQL_DELETE_ESTABLECIMIENTO =
            "DROP TABLE IF EXISTS " + Establecimiento.TABLE_NAME;
    public static final String SQL_DELETE_ANIMALENFERMO =
            "DROP TABLE IF EXISTS " + AnimalEnfermo.TABLE_NAME;
    public static final String SQL_DELETE_CUENTA =
            "DROP TABLE IF EXISTS " + Cuenta.TABLE_NAME;
    public static final String SQL_DELETE_DANIO =
            "DROP TABLE IF EXISTS " + Danio.TABLE_NAME;

    /* Inner class that defines the table contents */
    public static class Establecimiento implements BaseColumns {
        public static final String TABLE_NAME = "establecimiento";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_LATITUD = "latitud";
        public static final String COLUMN_NAME_LONGITUD = "longitud";
        public static final String COLUMN_NAME_DESCRIPCION = "descripcion";
    }
    public static class Cuenta implements BaseColumns {
        public static final String TABLE_NAME = "cuenta";
        public static final String EST_ID = "establecimiento";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_FECHA = "fecha";
        public static final String COLUMN_NAME_CUENTA = "cuenta";
        public static final String COLUMN_NAME_DESCRIPCION = "descripcion";
    }
    public static class AnimalEnfermo implements BaseColumns {
        public static final String TABLE_NAME = "animalenfermo";
        public static final String EST_ID = "establecimiento";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_FECHA = "fecha";
        public static final String COLUMN_NAME_FOTO = "foto";
        public static final String COLUMN_NAME_DESCRIPCION = "descripcion";
    }
    public static class Danio implements BaseColumns {
        public static final String TABLE_NAME = "danio";
        public static final String EST_ID = "establecimiento";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_FECHA = "fecha";
        public static final String COLUMN_NAME_FOTO = "foto";
        public static final String COLUMN_NAME_DESCRIPCION = "descripcion";
    }

}