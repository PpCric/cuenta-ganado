package com.campo.jose.cuentaganadoapp.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.campo.jose.cuentaganadoapp.AppConstant;
import com.campo.jose.cuentaganadoapp.R;
import com.campo.jose.cuentaganadoapp.bd.CuentaGanadoDbHelper;
import com.campo.jose.cuentaganadoapp.bean.AnimalEnfermo;
import com.campo.jose.cuentaganadoapp.bean.Danio;
import com.campo.jose.cuentaganadoapp.bean.Elemento;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;

import java.util.ArrayList;

public class DBElementoActivity extends AppCompatActivity {

    private ListView li;
    private ArrayList lista;
    private Establecimiento establecimiento;
    private String CHOICE = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbelemento);

        // Find the toolbar view inside the activity layout
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null

        Bundle datos = getIntent().getExtras();
        this.establecimiento = (Establecimiento) datos.getSerializable(AppConstant.ESTABLECIMIENTO);
        this.CHOICE = (String) datos.getString(AppConstant.CHOICE);

        li = (ListView) findViewById(R.id.listCount);

        ArrayAdapter adapter ;

        CuentaGanadoDbHelper db = new CuentaGanadoDbHelper(this);

        if (CHOICE.equals(AppConstant.ANIMAL)) {
            lista = db.getAnimalesEnfermos( establecimiento.getId());
            toolbar.setTitle("Animales");
        } else {
            lista = db.getDanios( establecimiento.getId());
            toolbar.setTitle("Daños");
        }


        toolbar.setSubtitle(establecimiento.getNombre());
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, lista);
        li.setAdapter(adapter);

        li.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                /*
                Intent myIntent = new Intent(view.getContext(), ElementoActivity.class);
                myIntent.putExtra(AppConstant.ELEMENTO,);
                myIntent.putExtra(AppConstant.CHOICE,CHOICE);
                myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                myIntent.putExtra(AppConstant.ACTION,AppConstant.EDIT);
                startActivityForResult(myIntent, 0);
                */
                llamarElementoActivty((Elemento)lista.get(position),AppConstant.EDIT);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Elemento elemento;
                if (CHOICE.equals(AppConstant.ANIMAL)) {
                    elemento = new AnimalEnfermo();
                } else {
                    elemento = new Danio();
                }
                elemento.setEstId(establecimiento.getId());
                /*
                Intent myIntent = new Intent(view.getContext(), ElementoActivity.class);
                myIntent.putExtra(AppConstant.ELEMENTO, elemento);
                myIntent.putExtra(AppConstant.CHOICE,CHOICE);
                myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                myIntent.putExtra(AppConstant.ACTION,AppConstant.CREATE);
                startActivityForResult(myIntent, 0);
                */
                llamarElementoActivty(elemento,AppConstant.CREATE);
            }
        });
    }
    private void llamarElementoActivty(Elemento elemento, String action){
        Intent myIntent = new Intent(getApplicationContext(), ElementoActivity.class);
        myIntent.putExtra(AppConstant.ELEMENTO,elemento);
        myIntent.putExtra(AppConstant.CHOICE,CHOICE);
        myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
        myIntent.putExtra(AppConstant.ACTION,action);
        startActivityForResult(myIntent, 0);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.putExtra(AppConstant.ESTABLECIMIENTO,this.establecimiento);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}