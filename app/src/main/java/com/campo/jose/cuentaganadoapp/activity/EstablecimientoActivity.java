package com.campo.jose.cuentaganadoapp.activity;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.campo.jose.cuentaganadoapp.AppConstant;
import com.campo.jose.cuentaganadoapp.R;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;

public class EstablecimientoActivity extends AppCompatActivity {

    private ListView funciones;
    private Establecimiento establecimiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_establecimiento);

        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        //setSupportActionBar(toolbar);

        Bundle datos = getIntent().getExtras();
        this.establecimiento = (Establecimiento) datos.getSerializable(AppConstant.ESTABLECIMIENTO);

        //toolbar.setTitle("Establecimiento "+this.establecimiento.getNombre());
        toolbar.setSubtitle(this.establecimiento.getNombre());
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);


        funciones = (ListView) findViewById(R.id.funciones);
        ArrayAdapter<CharSequence> adaptador = ArrayAdapter.createFromResource(this, R.array.funcion,android.R.layout.simple_list_item_1);
        funciones.setAdapter(adaptador);

        funciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                if (position == 0) {
                    Intent myIntent = new Intent(view.getContext(), DBElementoActivity.class);
                    myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);

                    myIntent.putExtra(AppConstant.CHOICE,AppConstant.ANIMAL);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 1) {
                    Intent myIntent = new Intent(view.getContext(), DBElementoActivity.class);
                    myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);

                    myIntent.putExtra(AppConstant.CHOICE,AppConstant.DANIO);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 2) {
                    Intent myIntent = new Intent(view.getContext(), DBCountsActivity.class);
                    myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                    startActivityForResult(myIntent, 0);
                }

                if (position == 3) {
                    Intent myIntent = new Intent(view.getContext(), ClimaActivity.class);
                    myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                    startActivityForResult(myIntent, 0);
                }
            }
        });

    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
}
