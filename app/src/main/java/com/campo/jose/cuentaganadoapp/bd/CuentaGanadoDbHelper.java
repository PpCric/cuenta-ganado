package com.campo.jose.cuentaganadoapp.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.campo.jose.cuentaganadoapp.bean.AnimalEnfermo;
import com.campo.jose.cuentaganadoapp.bean.Cuenta;
import com.campo.jose.cuentaganadoapp.bean.Danio;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;
import com.google.android.gms.maps.model.LatLng;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jose on 29/08/17.
 */

public class CuentaGanadoDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "CuentaGanado.db";

    public CuentaGanadoDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CuentaGanadoContract.SQL_CREATE_ESTABLECIMIENTO);
        db.execSQL(CuentaGanadoContract.SQL_CREATE_ANIMALENFERMO);
        db.execSQL(CuentaGanadoContract.SQL_CREATE_CUENTA);
        db.execSQL(CuentaGanadoContract.SQL_CREATE_DANIO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(CuentaGanadoContract.SQL_DELETE_ESTABLECIMIENTO);
        db.execSQL(CuentaGanadoContract.SQL_DELETE_ANIMALENFERMO);
        db.execSQL(CuentaGanadoContract.SQL_DELETE_CUENTA);
        db.execSQL(CuentaGanadoContract.SQL_DELETE_DANIO);
        onCreate(db);
    }

    public void close(){
        super.close();
    }

    public long insertEstablecimiento(Establecimiento establecimiento){
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(CuentaGanadoContract.Establecimiento.COLUMN_NAME_NOMBRE, establecimiento.getNombre());
        values.put(CuentaGanadoContract.Establecimiento.COLUMN_NAME_LATITUD, establecimiento.getCoordenadas().latitude);
        values.put(CuentaGanadoContract.Establecimiento.COLUMN_NAME_LONGITUD, establecimiento.getCoordenadas().longitude);
        values.put(CuentaGanadoContract.Establecimiento.COLUMN_NAME_DESCRIPCION, establecimiento.getDescripcion());
        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(CuentaGanadoContract.Establecimiento.TABLE_NAME, null, values);
        return newRowId;
    }

    public void insertCuenta(Cuenta cuenta){
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(CuentaGanadoContract.Cuenta.COLUMN_NAME_NOMBRE,cuenta.getNombre());
        values.put(CuentaGanadoContract.Cuenta.EST_ID,cuenta.getEstId());
        values.put(CuentaGanadoContract.Cuenta.COLUMN_NAME_FECHA,cuenta.getFecha());
        values.put(CuentaGanadoContract.Cuenta.COLUMN_NAME_CUENTA,cuenta.getCantidad());
        values.put(CuentaGanadoContract.Cuenta.COLUMN_NAME_DESCRIPCION, cuenta.getDescripcion());
        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(CuentaGanadoContract.Cuenta.TABLE_NAME, null, values);
    }

    public void insertAnimalEnfermo( AnimalEnfermo animalEnfermo){
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(CuentaGanadoContract.AnimalEnfermo.EST_ID,animalEnfermo.getEstId());
        values.put(CuentaGanadoContract.AnimalEnfermo.COLUMN_NAME_NOMBRE, animalEnfermo.getNombre());
        values.put(CuentaGanadoContract.AnimalEnfermo.COLUMN_NAME_FECHA, animalEnfermo.getFecha());
        values.put(CuentaGanadoContract.AnimalEnfermo.COLUMN_NAME_FOTO, animalEnfermo.getFotos().toString());
        values.put(CuentaGanadoContract.AnimalEnfermo.COLUMN_NAME_DESCRIPCION, animalEnfermo.getDescripcion());
        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(CuentaGanadoContract.AnimalEnfermo.TABLE_NAME, null, values);
    }

    public void insertDanio( Danio danio){
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(CuentaGanadoContract.Danio.EST_ID,danio.getEstId());
        values.put(CuentaGanadoContract.Danio.COLUMN_NAME_NOMBRE, danio.getNombre());
        values.put(CuentaGanadoContract.Danio.COLUMN_NAME_FECHA, danio.getFecha());
        values.put(CuentaGanadoContract.Danio.COLUMN_NAME_FOTO, danio.getFotos().toString());
        values.put(CuentaGanadoContract.Danio.COLUMN_NAME_DESCRIPCION, danio.getDescripcion());
        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(CuentaGanadoContract.Danio.TABLE_NAME, null, values);
    }

    public ArrayList<Establecimiento> getEstablecimientos(){
        ArrayList<Establecimiento> lista = new ArrayList<Establecimiento>();
        // Gets the data repository in write mode
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM "+CuentaGanadoContract.Establecimiento.TABLE_NAME,null);
        Establecimiento establecimiento;
        if (c.moveToFirst()){
            do{
                establecimiento = new Establecimiento();
                establecimiento.setId(Integer.parseInt(c.getString(0)));
                establecimiento.setNombre(c.getString(1));
                double lat = Double.parseDouble(c.getString(2));
                double lon = Double.parseDouble(c.getString(3));
                LatLng l = new LatLng(  lat,lon  );
                establecimiento.setCoordenadas(l);
                establecimiento.setDescripcion(c.getString(4));
                lista.add(establecimiento);
            }while (c.moveToNext());
        }
        return lista;
    }

    public ArrayList<Cuenta> getCuentas( long estId){
        ArrayList<Cuenta> lista = new ArrayList<Cuenta>();
        // Gets the data repository in write mode
        SQLiteDatabase db = getReadableDatabase();
        String[] args = new String[]{Long.toString(estId)};
        Cursor c = db.query(CuentaGanadoContract.Cuenta.TABLE_NAME, null, CuentaGanadoContract.Cuenta.EST_ID+" = ?", args, null, null, null);
        Cuenta cuenta;
        if (c.moveToFirst()){
            do{
                cuenta = new Cuenta();
                cuenta.setIdCuenta(Integer.parseInt(c.getString(0)));
                cuenta.setEstId(c.getInt(1));
                cuenta.setNombre(c.getString(2));
                DateFormat df = new SimpleDateFormat();
                try {
                    cuenta.setFecha(df.parse(c.getString(3)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cuenta.setCantidad(Integer.parseInt(c.getString(4)));
                cuenta.setDescripcion(c.getString(5));
                lista.add(cuenta);
            }while (c.moveToNext());
        }
        return lista;
    }

    public ArrayList<AnimalEnfermo> getAnimalesEnfermos( long estId) {
        ArrayList<AnimalEnfermo> lista = new ArrayList<AnimalEnfermo>();
        // Gets the data repository in write mode
        SQLiteDatabase db = getReadableDatabase();
        String[] args = new String[]{Long.toString(estId)};
        Cursor c = db.query(CuentaGanadoContract.AnimalEnfermo.TABLE_NAME, null, CuentaGanadoContract.AnimalEnfermo.EST_ID + "=?", args, null, null, null);
        AnimalEnfermo animalEnfermo;
        if (c.moveToFirst()){
            do{
                animalEnfermo = new AnimalEnfermo();
                animalEnfermo.setId(Integer.parseInt(c.getString(0)));
                animalEnfermo.setEstId(c.getInt(1));
                animalEnfermo.setNombre(c.getString(2));
                DateFormat df = new SimpleDateFormat();
                try {
                    animalEnfermo.setFecha(df.parse(c.getString(3)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                animalEnfermo.setFotos(getList(c.getString(4)));
                animalEnfermo.setDescripcion(c.getString(5));
                lista.add(animalEnfermo);
            }while (c.moveToNext());
        }
        return lista;
    }

    public ArrayList<Danio> getDanios( long estId) {
        ArrayList<Danio> lista = new ArrayList<Danio>();
        // Gets the data repository in write mode
        SQLiteDatabase db = getReadableDatabase();
        String[] args = new String[]{Long.toString(estId)};
        Cursor c = db.query(CuentaGanadoContract.Danio.TABLE_NAME, null, CuentaGanadoContract.Danio.EST_ID + "=?", args, null, null, null);
        Danio danio;
        if (c.moveToFirst()){
            do{
                danio = new Danio();
                danio.setId(Integer.parseInt(c.getString(0)));
                danio.setEstId(c.getInt(1));
                danio.setNombre(c.getString(2));
                DateFormat df = new SimpleDateFormat();
                try {
                    danio.setFecha(df.parse(c.getString(3)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                danio.setFotos(getList(c.getString(4)));
                danio.setDescripcion(c.getString(5));
                lista.add(danio);
            }while (c.moveToNext());
        }
        return lista;
    }

    public void editCuenta(Cuenta cuenta) {
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(CuentaGanadoContract.Cuenta.COLUMN_NAME_NOMBRE,cuenta.getNombre());
        values.put(CuentaGanadoContract.Cuenta.COLUMN_NAME_FECHA,cuenta.getFecha());
        values.put(CuentaGanadoContract.Cuenta.COLUMN_NAME_CUENTA,cuenta.getCantidad());
        values.put(CuentaGanadoContract.Cuenta.COLUMN_NAME_DESCRIPCION, cuenta.getDescripcion());
        String[] args = new String[]{Integer.toString(cuenta.getIdCuenta())};
        // Insert the new row, returning the primary key value of the new row
        long editRowId = db.update(CuentaGanadoContract.Cuenta.TABLE_NAME,values,CuentaGanadoContract.Cuenta._ID+" = ?",args);
    }

    public void editAnimalEnfermo( AnimalEnfermo animalEnfermo) {
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(CuentaGanadoContract.AnimalEnfermo.COLUMN_NAME_NOMBRE,animalEnfermo.getNombre());
        values.put(CuentaGanadoContract.AnimalEnfermo.COLUMN_NAME_FECHA,animalEnfermo.getFecha());
        values.put(CuentaGanadoContract.AnimalEnfermo.COLUMN_NAME_FOTO,animalEnfermo.getFotos().toString());
        values.put(CuentaGanadoContract.AnimalEnfermo.COLUMN_NAME_DESCRIPCION, animalEnfermo.getDescripcion());
        String[] args = new String[]{Integer.toString(animalEnfermo.getId())};
        // Insert the new row, returning the primary key value of the new row
        long editRowId = db.update(CuentaGanadoContract.AnimalEnfermo.TABLE_NAME,values,CuentaGanadoContract.AnimalEnfermo._ID+" = ?",args);
    }

    public void editDanio(Danio danio) {
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(CuentaGanadoContract.Danio.COLUMN_NAME_NOMBRE,danio.getNombre());
        values.put(CuentaGanadoContract.Danio.COLUMN_NAME_FECHA,danio.getFecha());
        values.put(CuentaGanadoContract.Danio.COLUMN_NAME_FOTO,danio.getFotos().toString());
        values.put(CuentaGanadoContract.Danio.COLUMN_NAME_DESCRIPCION, danio.getDescripcion());
        String[] args = new String[]{Integer.toString(danio.getId())};
        // Insert the new row, returning the primary key value of the new row
        long editRowId = db.update(CuentaGanadoContract.Danio.TABLE_NAME,values,CuentaGanadoContract.Danio._ID+" = ?",args);
    }

    //[JPEG_20180220_141845_-736747064.jpg, JPEG_20180220_141850_-1761652009.jpg]
    private List<String> getList(String list){
        return Arrays.asList(list.replace(" ","").replace("[","").replace("]","").split(","));
    }
}
