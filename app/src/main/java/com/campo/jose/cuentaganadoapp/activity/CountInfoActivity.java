package com.campo.jose.cuentaganadoapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.campo.jose.cuentaganadoapp.AppConstant;
import com.campo.jose.cuentaganadoapp.R;
import com.campo.jose.cuentaganadoapp.bd.CuentaGanadoDbHelper;
import com.campo.jose.cuentaganadoapp.bean.Cuenta;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;

public class CountInfoActivity extends AppCompatActivity {

    private EditText nombre = null;
    private EditText descripcion = null;
    private TextView cuentaTV = null;
    private TextView fechaTV = null;
    private Cuenta cuenta = null;
    private Establecimiento establecimiento = null;
    private Button ok = null;
    private String action = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_info);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Bundle datos = getIntent().getExtras();
        this.establecimiento = (Establecimiento) datos.getSerializable(AppConstant.ESTABLECIMIENTO);
        this.cuenta = (Cuenta)datos.getSerializable(AppConstant.CUENTA);
        this.action = (String)datos.getString(AppConstant.ACTION);

        this.cuentaTV = (TextView) findViewById(R.id.texto_nrocuenta);
        this.cuentaTV.setText(this.cuenta.getCantidad());

        this.fechaTV = (TextView) findViewById(R.id.fecha_hoy);
        this.fechaTV.setText(cuenta.getFecha());

        this.nombre = (EditText)findViewById(R.id.nombre);
        this.descripcion = (EditText)findViewById(R.id.descripcion);
        // Caso que esta editando
        if (cuenta.getNombre()!=null){
            this.nombre.setText(cuenta.getNombre());

            toolbar.setSubtitle(cuenta.getNombre());
        }
        if (cuenta.getDescripcion()!=null){
            this.descripcion.setText(cuenta.getDescripcion());
        }

        this.ok = (Button) findViewById(R.id.ok);
        this.ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cuenta.setNombre(nombre.getText().toString());
                cuenta.setDescripcion(descripcion.getText().toString());

                CuentaGanadoDbHelper db = new CuentaGanadoDbHelper(getApplicationContext());
                if (action.equals(AppConstant.CREATE)) {
                    db.insertCuenta(cuenta);
                } else if (action.equals(AppConstant.EDIT)){
                    db.editCuenta(cuenta);
                }

                Intent myIntent = new Intent(v.getContext(), DBCountsActivity.class);

                myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);

                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(myIntent, 0);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.putExtra(AppConstant.ESTABLECIMIENTO,this.establecimiento);
                upIntent.putExtra(AppConstant.CUENTA,this.cuenta);
                upIntent.putExtra(AppConstant.ACTION,this.action);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
