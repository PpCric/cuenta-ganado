package com.campo.jose.cuentaganadoapp.activity;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.campo.jose.cuentaganadoapp.AppConstant;
import com.campo.jose.cuentaganadoapp.R;
import com.campo.jose.cuentaganadoapp.bd.CuentaGanadoDbHelper;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;
import com.campo.jose.cuentaganadoapp.util.SimpleContact;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView establecimientosListView;
    private ArrayList<Establecimiento> lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null

        toolbar.setSubtitle("Establecimientos");
        setSupportActionBar(toolbar);

        /*
        final AccountManager manager = AccountManager.get(this);
        final Account[] accounts;
        accounts = manager.getAccountsByType("com.google");

        String[] emails = new String[accounts.length];
        for (int i = 0; i < accounts.length; i++) {
            emails[i] = accounts[i].name;
        }
        */

        //SimpleContact sc = SimpleContact.getLocalProfile(this);

        int actualizado = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS !=  actualizado){

        };

        establecimientosListView = (ListView) findViewById(R.id.establecimientos);

        CuentaGanadoDbHelper db = new CuentaGanadoDbHelper(this);

        lista = db.getEstablecimientos();

        ArrayAdapter<Establecimiento> adapter = new ArrayAdapter<Establecimiento>(this,android.R.layout.simple_list_item_1,lista);
        establecimientosListView.setAdapter(adapter);

        establecimientosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Establecimiento e = lista.get(position);
                Intent myIntent = new Intent(view.getContext(), EstablecimientoActivity.class);
                myIntent.putExtra(AppConstant.ESTABLECIMIENTO,e);
                startActivityForResult(myIntent, 0);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent myIntent = new Intent(view.getContext(), MapsEstablecimientoActivity.class);
                startActivityForResult(myIntent, 0);
                /*
                Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                */
            }
        });

    }



}
