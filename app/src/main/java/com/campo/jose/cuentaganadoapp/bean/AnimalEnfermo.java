package com.campo.jose.cuentaganadoapp.bean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Created by jose on 06/10/17.
 */

public class AnimalEnfermo extends Elemento {

    public AnimalEnfermo(){
        this.fotos = new ArrayList<>();
        this.fecha = new Date();
    }
}
