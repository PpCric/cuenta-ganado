package com.campo.jose.cuentaganadoapp.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.campo.jose.cuentaganadoapp.AppConstant;
import com.campo.jose.cuentaganadoapp.R;
import com.campo.jose.cuentaganadoapp.bd.CuentaGanadoDbHelper;
import com.campo.jose.cuentaganadoapp.bean.AnimalEnfermo;
import com.campo.jose.cuentaganadoapp.bean.Danio;
import com.campo.jose.cuentaganadoapp.bean.Elemento;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ElementoActivity extends AppCompatActivity {

    static final int REQUEST_TAKE_PHOTO = 1;
    private String mCurrentPhotoPath;
    private File storageDir;

    private EditText nombreText = null;
    private EditText descripcionText = null;
    private TextView fechaTV = null;

    private Button sacarFoto = null;
    private Button verFotos = null;
    private Button ok = null;

    private String action = null;
    private String choice = null;
    private ImageView mImageView= null;


    private Elemento elemento = null;
    private Establecimiento establecimiento = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elemento);

        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        Bundle datos = getIntent().getExtras();

        this.establecimiento = (Establecimiento) datos.getSerializable(AppConstant.ESTABLECIMIENTO);
        this.action = (String)datos.getString(AppConstant.ACTION);
        this.choice = (String)datos.getString(AppConstant.CHOICE);
        this.elemento = (Elemento) datos.getSerializable(AppConstant.ELEMENTO);

        String preTitle = null;
        String title = null;
        String fecha = null;
        String nombre = null;
        String descripcion = null;
        String fotoDir = null;

        if(choice.equals(AppConstant.ANIMAL)){
            preTitle = "Animal Enfermo ";
        } else {
            preTitle = "Daño ";
        }

        if (this.elemento.getNombre() != null) {
            title =  this.elemento.getNombre();
        } else {
            title = "Nuevo";
        }
        fecha = this.elemento.getFecha();
        if (this.elemento.getNombre()!=null) {
            nombre = this.elemento.getNombre();
        }
        if (this.elemento.getDescripcion()!=null){
            descripcion = this.elemento.getDescripcion();
        }
        if (elemento.getFotos().size()>0){
            fotoDir = (String)elemento.getFotos().get(0);
        }

        toolbar.setTitle(preTitle);
        toolbar.setSubtitle(title);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        /*
            Directorio donde se alojan las fotos
         */
        this.storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        this.nombreText = (EditText) findViewById(R.id.nombre);
        this.descripcionText = (EditText) findViewById(R.id.descripcion);
        this.fechaTV = (TextView) findViewById(R.id.fecha_hoy);

        this.fechaTV.setText(fecha);
        this.nombreText.setText(nombre);
        this.descripcionText.setText(descripcion);

        this.ok = (Button) findViewById(R.id.ok);

        this.ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                elemento.setNombre(nombreText.getText().toString());
                elemento.setDescripcion(descripcionText.getText().toString());

                CuentaGanadoDbHelper db = new CuentaGanadoDbHelper(getApplicationContext());

                if (choice.equals(AppConstant.ANIMAL)) {
                    if (action.equals(AppConstant.CREATE)) {
                        db.insertAnimalEnfermo( (AnimalEnfermo) elemento);
                    } else if (action.equals(AppConstant.EDIT)) {
                        db.editAnimalEnfermo( (AnimalEnfermo) elemento);
                    }
                } else {
                    if (action.equals(AppConstant.CREATE)) {
                        db.insertDanio( (Danio)elemento);
                    } else if (action.equals(AppConstant.EDIT)) {
                        db.editDanio( (Danio)elemento);
                    }
                }
                if (action.equals(AppConstant.CREATE)){
                    Toast.makeText(getApplicationContext(),"Se agrego " + elemento.getNombre() + " a "+establecimiento.getNombre(),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(),"Se guardo " + elemento.getNombre(),
                            Toast.LENGTH_SHORT).show();
                }

                Intent myIntent = new Intent(v.getContext(), DBElementoActivity.class);
                myIntent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                myIntent.putExtra(AppConstant.CHOICE,choice);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(myIntent, 0);
            }
        });

        this.mImageView = (ImageView)findViewById(R.id.imageView);

        Glide.with(this).load(storageDir+"/"+fotoDir)
                    .centerCrop()
                    .fitCenter()
                    .into(mImageView);

        this.sacarFoto = (Button)findViewById(R.id.camara);
        this.sacarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        this.verFotos = (Button)findViewById(R.id.verfotos);
        this.verFotos.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), FotosListActivity.class);
                intent.putExtra(AppConstant.ELEMENTO, (Serializable) elemento);
                intent.putExtra(AppConstant.CHOICE,choice);
                intent.putExtra(AppConstant.ESTABLECIMIENTO,establecimiento);
                intent.putExtra(AppConstant.ACTION,action);
                startActivity(intent);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            /*Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            mImageView.setImageBitmap(imageBitmap);*/
            galleryAddPic();
            setPic();
        }
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.campo.jose.cuentaganadoapp.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir =
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        /*
            Almaceno el nombre de la foto en el animalEnfermo
         */
        this.elemento.addFoto(image.getName());
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    /*
        Toma la foto que recien se capturo y la carga en la pantalla
     */
    private void setPic() {
        // Get the dimensions of the View

        Glide.with(this).load(mCurrentPhotoPath)
                .centerCrop()
                .fitCenter()
                .into(mImageView);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.putExtra(AppConstant.ESTABLECIMIENTO,this.establecimiento);
                upIntent.putExtra(AppConstant.ACTION,this.action);
                upIntent.putExtra(AppConstant.CHOICE,this.choice);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
