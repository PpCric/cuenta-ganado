package com.campo.jose.cuentaganadoapp.activity;

import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.campo.jose.cuentaganadoapp.AppConstant;
import com.campo.jose.cuentaganadoapp.R;
import com.campo.jose.cuentaganadoapp.bean.Elemento;
import com.campo.jose.cuentaganadoapp.bean.Establecimiento;

public class FotoViewerActivity extends AppCompatActivity {

    private String foto = null;
    private ImageView imageView = null;
    private Establecimiento establecimiento = null;
    private Elemento elemento = null;
    private String action = null;
    private String choice = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto_viewer);

        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);


        Bundle datos = getIntent().getExtras();
        this.establecimiento = (Establecimiento) datos.getSerializable(AppConstant.ESTABLECIMIENTO);
        this.action = datos.getString(AppConstant.ACTION);
        this.choice = datos.getString(AppConstant.CHOICE);
        this.foto =  datos.getString("foto");
        this.elemento = (Elemento) datos.getSerializable(AppConstant.ELEMENTO);

        imageView = (ImageView)findViewById(R.id.imageView2);
        //byte[] fbyte = Manager.getINSTANCE().getFoto(Long.parseLong(foto));


        //Bitmap bitmap = BitmapFactory.decodeByteArray(fbyte,0,fbyte.length);
        String foto = getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath()+"/"+this.foto;

        Glide.with(this).load( foto)
                .into(imageView);
        //Bitmap bitmap = BitmapFactory.decodeFile(foto);
        //imageView.setImageBitmap(bitmap);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.putExtra(AppConstant.ELEMENTO,this.elemento);
                upIntent.putExtra(AppConstant.ESTABLECIMIENTO,this.establecimiento);
                upIntent.putExtra(AppConstant.ACTION,this.action);
                upIntent.putExtra(AppConstant.CHOICE,this.choice);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
